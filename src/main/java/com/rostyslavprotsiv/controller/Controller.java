package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.WorkflowDemonstration;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final Menu MENU = new Menu();
    private static final WorkflowDemonstration DEMONSTRATION
            = new WorkflowDemonstration();

    public void execute() {
        MENU.welcome();
        MENU.outDemonstration();
        DEMONSTRATION.demonstrate();
    }
}
