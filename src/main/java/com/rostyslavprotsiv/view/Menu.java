package com.rostyslavprotsiv.view;

import com.rostyslavprotsiv.model.action.WorkflowDemonstration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Welcome to my program!!");
    }

    public void outDemonstration() {
        LOGGER.info("Demonstration the factory method working:");
    }

}
