package com.rostyslavprotsiv.model.exception;

public class CheesePizzaLogicalException extends Exception {
    public CheesePizzaLogicalException() {
    }

    public CheesePizzaLogicalException(String s) {
        super(s);
    }

    public CheesePizzaLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public CheesePizzaLogicalException(Throwable throwable) {
        super(throwable);
    }
}
