package com.rostyslavprotsiv.model.entity;

public enum PizzaType {
    CHEESE, VEGGIE, CLAM, PEPPERONI
}
