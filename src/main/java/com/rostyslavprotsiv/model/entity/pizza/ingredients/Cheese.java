package com.rostyslavprotsiv.model.entity.pizza.ingredients;

public enum Cheese {
    MOZZARELLA, CHEDDAR, FETA, BRIE
}
