package com.rostyslavprotsiv.model.entity.pizza.ingredients;

public enum Dough {
    THICK, THIN
}
