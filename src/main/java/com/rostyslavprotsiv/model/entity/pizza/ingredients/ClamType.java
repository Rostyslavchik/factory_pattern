package com.rostyslavprotsiv.model.entity.pizza.ingredients;

public enum ClamType {
    CHOWDER, CHERRYSTONE
}
