package com.rostyslavprotsiv.model.entity.pizza;

import com.rostyslavprotsiv.model.entity.pizza.ingredients.Dough;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Sauce;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Topping;

import java.util.List;
import java.util.Objects;

public abstract class Pizza implements PizzaMaker {
    protected Dough dough;
    protected Sauce baseSauce;
    protected List<Topping> toppings;

    public Pizza() {}

    public Pizza(Dough dough, Sauce baseSauce, List<Topping> toppings) {
        this.dough = dough;
        this.baseSauce = baseSauce;
        this.toppings = toppings;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public Sauce getBaseSauce() {
        return baseSauce;
    }

    public void setBaseSauce(Sauce baseSauce) {
        this.baseSauce = baseSauce;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    public abstract String getSpecialIngredient();

    @Override
    public void prepare() {
        System.out.println("Preparing the " + this.getClass().getSimpleName());
    }

    @Override
    public void make() {
        System.out.println("Making the " + this.getClass().getSimpleName());
    }

    @Override
    public void cut() {
        System.out.println("Cutting the " + this.getClass().getSimpleName());
    }

    @Override
    public void box() {
        System.out.println("Boxing the " + this.getClass().getSimpleName());
    }

    @Override
    public void unbox() {
        System.out.println("Unboxing the " + this.getClass().getSimpleName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return dough == pizza.dough && baseSauce == pizza.baseSauce && Objects.equals(toppings, pizza.toppings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dough, baseSauce, toppings);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "dough=" + dough +
                ", baseSauce=" + baseSauce +
                ", toppings=" + toppings;
    }
}
