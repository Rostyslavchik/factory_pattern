package com.rostyslavprotsiv.model.entity.pizza.ingredients;

public enum Sauce {
    MARINARA, PLUM_TOMATO, PESTO, MAYO
}
