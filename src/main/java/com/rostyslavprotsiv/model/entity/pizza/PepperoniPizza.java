package com.rostyslavprotsiv.model.entity.pizza;

import com.rostyslavprotsiv.model.entity.pizza.ingredients.Dough;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Sauce;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Topping;

import java.util.List;
import java.util.Objects;

public class PepperoniPizza extends Pizza {
    private boolean isHot;
    private boolean isChilePepperIncluded;

    public PepperoniPizza() {
    }

    public PepperoniPizza(Dough dough, Sauce baseSauce, List<Topping> toppings,
                          boolean isHot, boolean isChilePepperIncluded) {
        super(dough, baseSauce, toppings);
        this.isHot = isHot;
        this.isChilePepperIncluded = isChilePepperIncluded;
    }

    public boolean isHot() {
        return isHot;
    }

    public void setHot(boolean hot) {
        isHot = hot;
    }

    public boolean isChilePepperIncluded() {
        return isChilePepperIncluded;
    }

    public void setChilePepperIncluded(boolean chilePepperIncluded) {
        isChilePepperIncluded = chilePepperIncluded;
    }

    @Override
    public String getSpecialIngredient() {
        return "The red pepper is included: " + isChilePepperIncluded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PepperoniPizza that = (PepperoniPizza) o;
        return isHot == that.isHot && isChilePepperIncluded == that.isChilePepperIncluded;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isHot, isChilePepperIncluded);
    }

    @Override
    public String toString() {
        return "PepperoniPizza{" +
                "isHot=" + isHot +
                ", isChilePepperIncluded=" + isChilePepperIncluded +
                ", dough=" + dough +
                ", baseSauce=" + baseSauce +
                ", toppings=" + toppings +
                '}';
    }
}
