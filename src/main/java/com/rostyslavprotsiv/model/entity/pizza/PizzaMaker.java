package com.rostyslavprotsiv.model.entity.pizza;

public interface PizzaMaker {
    void prepare();
    void make();
    void cut();
    void box();
    void unbox();
}
