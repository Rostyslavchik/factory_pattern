package com.rostyslavprotsiv.model.entity.pizza;

import com.rostyslavprotsiv.model.entity.pizza.ingredients.Dough;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Sauce;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Topping;

import java.util.List;
import java.util.Objects;

public class VeggiePizza extends Pizza {
    private String saladColor;

    public VeggiePizza() {
    }

    public VeggiePizza(Dough dough, Sauce baseSauce, List<Topping> toppings,
                       String saladColor) {
        super(dough, baseSauce, toppings);
        this.saladColor = saladColor;
    }

    public String getSaladColor() {
        return saladColor;
    }

    public void setSaladColor(String saladColor) {
        this.saladColor = saladColor;
    }

    @Override
    public String getSpecialIngredient() {
        return "Salad color is" + saladColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        VeggiePizza that = (VeggiePizza) o;
        return Objects.equals(saladColor, that.saladColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), saladColor);
    }

    @Override
    public String toString() {
        return "VeggiePizza{" +
                "dough=" + dough +
                ", baseSauce=" + baseSauce +
                ", toppings=" + toppings +
                ", saladColor='" + saladColor + '\'' +
                '}';
    }
}
