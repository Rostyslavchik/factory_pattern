package com.rostyslavprotsiv.model.entity.pizza;

import com.rostyslavprotsiv.model.entity.pizza.ingredients.Cheese;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Dough;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Sauce;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Topping;
import com.rostyslavprotsiv.model.exception.CheesePizzaLogicalException;

import java.util.List;
import java.util.Objects;

public final class CheesePizza extends Pizza {
    private Cheese cheese;
    private int cheeseWeight;

    public CheesePizza() {}

    public CheesePizza(Dough dough, Sauce baseSauce, List<Topping> toppings,
                       Cheese cheese, int cheeseWeight)
            throws CheesePizzaLogicalException {
        super(dough, baseSauce, toppings);
        if (checkCheeseWeight(cheeseWeight)) {
            this.cheese = cheese;
            this.cheeseWeight = cheeseWeight;
        } else {
            throw new CheesePizzaLogicalException("Weight of cheese is <= 0");
        }
    }

    public Cheese getCheese() {
        return cheese;
    }

    public void setCheese(Cheese cheese) {
        this.cheese = cheese;
    }

    public int getCheeseWeight() {
        return cheeseWeight;
    }

    public void setCheeseWeight(int cheeseWeight)
            throws CheesePizzaLogicalException {
        if (checkCheeseWeight(cheeseWeight)) {
            this.cheeseWeight = cheeseWeight;
        } else {
            throw new CheesePizzaLogicalException("Weight of cheese is <= 0");
        }
    }

    @Override
    public String getSpecialIngredient() {
        return cheese.toString();
    }

    private boolean checkCheeseWeight(int cheeseWeight) {
        return cheeseWeight > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CheesePizza that = (CheesePizza) o;
        return cheeseWeight == that.cheeseWeight && cheese == that.cheese;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cheese, cheeseWeight);
    }

    @Override
    public String toString() {
        return "CheesePizza{" +
                "cheese=" + cheese +
                ", cheeseWeight=" + cheeseWeight +
                ", dough=" + dough +
                ", baseSauce=" + baseSauce +
                ", toppings=" + toppings +
                '}';
    }
}
