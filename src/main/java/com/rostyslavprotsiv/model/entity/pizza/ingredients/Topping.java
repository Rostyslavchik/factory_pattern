package com.rostyslavprotsiv.model.entity.pizza.ingredients;

public enum Topping {
    TOMATO, CHICKEN, PEPPER, SAUSAGE, SALAD, MUSTARD, MUSHROOM, OLIVE
}
