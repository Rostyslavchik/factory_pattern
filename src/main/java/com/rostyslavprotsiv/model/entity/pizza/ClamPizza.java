package com.rostyslavprotsiv.model.entity.pizza;

import com.rostyslavprotsiv.model.entity.pizza.ingredients.ClamType;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Dough;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Sauce;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.Topping;

import java.util.List;
import java.util.Objects;

public class ClamPizza extends Pizza {
    private ClamType clamType;

    public ClamPizza() {
    }

    public ClamPizza(Dough dough, Sauce baseSauce,
                     List<Topping> toppings, ClamType clamType) {
        super(dough, baseSauce, toppings);
        this.clamType = clamType;
    }

    public ClamType getClamType() {
        return clamType;
    }

    public void setClamType(ClamType clamType) {
        this.clamType = clamType;
    }

    @Override
    public String getSpecialIngredient() {
        return clamType.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ClamPizza clamPizza = (ClamPizza) o;
        return clamType == clamPizza.clamType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), clamType);
    }

    @Override
    public String toString() {
        return "ClamPizza{" +
                "clamType=" + clamType +
                ", dough=" + dough +
                ", baseSauce=" + baseSauce +
                ", toppings=" + toppings +
                '}';
    }
}
