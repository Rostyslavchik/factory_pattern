package com.rostyslavprotsiv.model.action.bakery;

import com.rostyslavprotsiv.model.entity.PizzaType;
import com.rostyslavprotsiv.model.entity.pizza.*;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.*;
import com.rostyslavprotsiv.model.exception.CheesePizzaLogicalException;

import java.util.Arrays;

public class KyivBakery extends Bakery {
    @Override
    protected PizzaMaker createPizza(PizzaType type) {
        PizzaMaker pizza = null;
        switch (type) {
            case CHEESE:
                try {
                    pizza = new CheesePizza(Dough.THIN, Sauce.MARINARA,
                            Arrays.asList(Topping.MUSHROOM,
                                    Topping.CHICKEN,
                                    Topping.TOMATO), Cheese.BRIE, 125);
                } catch (CheesePizzaLogicalException e) {
                    e.printStackTrace();
                }
                break;
            case CLAM:
                pizza = new ClamPizza(Dough.THICK, Sauce.MAYO,
                        Arrays.asList(Topping.SALAD,
                                Topping.OLIVE,
                                Topping.TOMATO), ClamType.CHERRYSTONE);
                break;
            case VEGGIE:
                pizza = new VeggiePizza(Dough.THIN, Sauce.MARINARA,
                        Arrays.asList(Topping.SALAD,
                                Topping.OLIVE,
                                Topping.TOMATO,
                                Topping.MUSTARD), "black");
                break;
            case PEPPERONI:
                pizza = new PepperoniPizza(Dough.THIN, Sauce.MAYO,
                        Arrays.asList(
                                Topping.PEPPER,
                                Topping.CHICKEN,
                                Topping.MUSTARD), true, false);
                break;
        }
        return pizza;
    }
}
