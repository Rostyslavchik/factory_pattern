package com.rostyslavprotsiv.model.action.bakery;

import com.rostyslavprotsiv.model.entity.PizzaType;
import com.rostyslavprotsiv.model.entity.pizza.*;
import com.rostyslavprotsiv.model.entity.pizza.ingredients.*;
import com.rostyslavprotsiv.model.exception.CheesePizzaLogicalException;

import java.util.Arrays;

public class DniproBakery extends Bakery {
    @Override
    protected PizzaMaker createPizza(PizzaType type) {
        PizzaMaker pizza = null;
        switch (type) {
            case CHEESE:
                try {
                    pizza = new CheesePizza(Dough.THICK, Sauce.MARINARA,
                            Arrays.asList(Topping.MUSHROOM,
                                    Topping.SAUSAGE,
                                    Topping.TOMATO), Cheese.FETA, 140);
                } catch (CheesePizzaLogicalException e) {
                    e.printStackTrace();
                }
                break;
            case CLAM:
                pizza = new ClamPizza(Dough.THICK, Sauce.PESTO,
                        Arrays.asList(Topping.OLIVE,
                                Topping.OLIVE,
                                Topping.TOMATO), ClamType.CHOWDER);
                break;
            case VEGGIE:
                pizza = new VeggiePizza(Dough.THIN, Sauce.PLUM_TOMATO,
                        Arrays.asList(Topping.SALAD,
                                Topping.OLIVE,
                                Topping.TOMATO,
                                Topping.MUSTARD), "green");
                break;
            case PEPPERONI:
                pizza = new PepperoniPizza(Dough.THIN, Sauce.PESTO,
                        Arrays.asList(
                                Topping.PEPPER,
                                Topping.PEPPER,
                                Topping.CHICKEN,
                                Topping.MUSHROOM,
                                Topping.MUSTARD), true, true);
                break;
        }
        return pizza;
    }
}
