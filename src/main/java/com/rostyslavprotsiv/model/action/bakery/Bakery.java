package com.rostyslavprotsiv.model.action.bakery;

import com.rostyslavprotsiv.model.entity.PizzaType;
import com.rostyslavprotsiv.model.entity.pizza.PizzaMaker;

public abstract class Bakery {
    protected abstract PizzaMaker createPizza(PizzaType type);

    public PizzaMaker executeOrder(PizzaType type) {
        PizzaMaker pizza = createPizza(type);
        pizza.prepare();
        pizza.make();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
