package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.action.bakery.Bakery;
import com.rostyslavprotsiv.model.action.bakery.DniproBakery;
import com.rostyslavprotsiv.model.action.bakery.KyivBakery;
import com.rostyslavprotsiv.model.action.bakery.LvivBakery;
import com.rostyslavprotsiv.model.entity.PizzaType;
import com.rostyslavprotsiv.model.entity.pizza.PizzaMaker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorkflowDemonstration {
    private static final Logger LOGGER = LogManager.getLogger(
            WorkflowDemonstration.class);

    public void demonstrate() {
        LOGGER.info("CheesePizzas demonstration: (Lviv vs Kyiv)");
        Bakery bakery = new LvivBakery();
        PizzaMaker pizza = bakery.executeOrder(PizzaType.CHEESE);
        pizza.unbox();
        LOGGER.info(pizza);
        bakery = new KyivBakery();
        pizza = bakery.executeOrder(PizzaType.CHEESE);
        pizza.unbox();
        LOGGER.info(pizza);
        LOGGER.info("PepperoniPizzas demonstration: (Lviv vs Dnipro)");
        bakery = new LvivBakery();
        pizza = bakery.executeOrder(PizzaType.PEPPERONI);
        pizza.unbox();
        LOGGER.info(pizza);
        bakery = new DniproBakery();
        pizza = bakery.executeOrder(PizzaType.PEPPERONI);
        pizza.unbox();
        LOGGER.info(pizza);
    }
}
